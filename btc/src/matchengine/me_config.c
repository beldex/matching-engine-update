/*
 * Description: 
 *     History: yang@haipo.me, 2017/03/16, create
 */

# include "me_config.h"
# include "me_server.h"

struct settings settings;

static int load_assets()
{
    MYSQL *conn = mysql_connect(&settings.db_assets);

    if (conn == NULL) {
        log_error ("MYSQL CONNECT FAIL TO ASSETS_ADD DB");
        log_stderr("MYSQL CONNECT FAIL TO ASSETS_ADD DB");
        return -__LINE__;
    }

    else{
        log_trace("ASSETS_ADD DB CONNECTED...");
    }

    sds sql = sdsempty();
        sql = sdscatprintf(sql, "SELECT * FROM assets");
        log_trace ("EXEC SQL [%s]", sql);

    int ret = mysql_real_query(conn, sql, sdslen(sql));

    if (ret != 0) {
        log_error ("EXEC SQL [%s] FAIL [%d] [%s]", sql, mysql_errno(conn), mysql_error(conn));
        log_stderr("EXEC SQL [%s] FAIL [%d] [%s]", sql, mysql_errno(conn), mysql_error(conn));
        sdsfree(sql);
        return -__LINE__;
    }

    sdsfree(sql);
    MYSQL_RES *result = mysql_store_result(conn);
    size_t num_rows = mysql_num_rows(result);
    settings.asset_num = num_rows;
    log_trace("settings.asset_num [%d]",settings.asset_num);
    settings.assets = malloc(sizeof(struct asset) * settings.asset_num);
    //settings.assets = realloc(settings.assets, sizeof(struct asset) * ( settings.asset_num + 1) );
    for (size_t i = 0; i < settings.asset_num ; ++i)
    {
        MYSQL_ROW row = mysql_fetch_row(result);
        settings.assets[i].name = strdup(row[1]);
        settings.assets[i].prec_save = atoi(row[2]);
        settings.assets[i].prec_show = atoi(row[3]);

        log_trace ("NAME      [%s]",settings.assets[i].name);
        log_trace ("PREC_SAVE [%d]",settings.assets[i].prec_save);
        log_trace ("PREC_SHOW [%d]",settings.assets[i].prec_show);
        log_trace ("\n");
    }
    mysql_free_result(result);
    mysql_close(conn);
    return 0;
}

static int load_markets()
{
    MYSQL *conn = mysql_connect(&settings.db_markets);

    if (conn == NULL) {
        log_error ("MYSQL CONNECT FAIL TO MARKETS_ADD DB");
        log_stderr("MYSQL CONNECT FAIL TO MARKETS_ADD DB");
        return -__LINE__;
    }

    else{
        log_trace ("MARKETS_ADD DB CONNECTED...");
    }

    sds sql = sdsempty();
        sql = sdscatprintf(sql, "SELECT * FROM markets");
        log_trace ("EXEC SQL  [%s]", sql);

    int ret = mysql_real_query(conn, sql, sdslen(sql));
    if (ret != 0) {
        log_error ("EXEC SQL [%s] FAIL [%d] [%s]", sql, mysql_errno(conn), mysql_error(conn));
        log_stderr("EXEC SQL [%s] FAIL [%d] [%s]", sql, mysql_errno(conn), mysql_error(conn));
        sdsfree(sql);
        return -__LINE__;
    }
    sdsfree(sql);
    MYSQL_RES *result = mysql_store_result(conn);
    size_t num_rows = mysql_num_rows(result);
    settings.market_num = num_rows;
    log_trace("settings.market_num [%d]",settings.market_num);
    settings.markets = malloc(sizeof(struct market) * settings.market_num);
    //settings.markets = realloc(settings.markets, sizeof(struct market) * ( settings.market_num + 1) );
     for (size_t i = 0; i < settings.market_num; ++i)
    {
        MYSQL_ROW row = mysql_fetch_row(result);
        settings.markets[i].name = strdup(row[1]);
        settings.markets[i].fee_prec = atoi(row[2]);
        settings.markets[i].min_amount = decimal(row[3],0.001);
        settings.markets[i].stock = strdup(row[4]);
        settings.markets[i].stock_prec = atoi(row[5]);
        settings.markets[i].money = strdup(row[6]);
        settings.markets[i].money_prec = atoi(row[7]);

        log_trace ("NAME      [%s]",settings.markets[i].name);
        log_trace ("FEE_PREC  [%d]",settings.markets[i].fee_prec);
        log_trace ("MIN_AMOUNT[%s]",row[3]);
        log_trace ("STOCK     [%s]",settings.markets[i].stock);
        log_trace ("STOCK_PREC[%d]",settings.markets[i].stock_prec);
        log_trace ("MONEY         [%s]",settings.markets[i].money);
        log_trace ("MONEY_PREC[%d]",settings.markets[i].money_prec);
        log_trace ("\n");
    }
    mysql_free_result(result);
    mysql_close(conn);
    return 0;
}

static int read_config_from_json(json_t *root)
{
    int ret;
    ret = read_cfg_bool(root, "debug", &settings.debug, false, false);
    if (ret < 0) {
        printf("read debug config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_process(root, "process", &settings.process);
    if (ret < 0) {
        printf("load process config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_log(root, "log", &settings.log);
    if (ret < 0) {
        printf("load log config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_alert(root, "alert", &settings.alert);
    if (ret < 0) {
        printf("load alert config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_rpc_svr(root, "svr", &settings.svr);
    if (ret < 0) {
        printf("load svr config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_cli_svr(root, "cli", &settings.cli);
    if (ret < 0) {
        printf("load cli config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_mysql(root, "db_log", &settings.db_log);
    if (ret < 0) {
        printf("load log db config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_mysql(root, "db_history", &settings.db_history);
    if (ret < 0) {
        printf("load history db config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_mysql(root, "db_assets", &settings.db_assets);
    if (ret < 0)
    {
        printf("load assets db config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_cfg_mysql(root, "db_markets", &settings.db_markets);
    if (ret < 0)
    {
        printf("load markets db config fail: %d\n", ret);
        return -__LINE__;
    }

    /*ret = load_assets(root, "assets");
    if (ret < 0) {
        printf("load assets config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_markets(root, "markets");
    if (ret < 0) {
        printf("load markets config fail: %d\n", ret);
        return -__LINE__;
    }*/
    ret = load_assets();
    if (ret < 0)
    {
        printf("load assets config fail: %d\n", ret);
        return -__LINE__;
    }
    ret = load_markets();
    if (ret < 0)
    {
        printf("load markets config fail: %d\n", ret);
        return -__LINE__;
    }

    ret = read_cfg_str(root, "brokers", &settings.brokers, NULL);
    if (ret < 0) {
        printf("load brokers fail: %d\n", ret);
        return -__LINE__;
    }
    ret = read_cfg_int(root, "slice_interval", &settings.slice_interval, false, 86400);
    if (ret < 0) {
        printf("load slice_interval fail: %d", ret);
        return -__LINE__;
    }
    ret = read_cfg_int(root, "slice_keeptime", &settings.slice_keeptime, false, 86400 * 3);
    if (ret < 0) {
        printf("load slice_keeptime fail: %d", ret);
        return -__LINE__;
    }
    ret = read_cfg_int(root, "history_thread", &settings.history_thread, false, 10);
    if (ret < 0) {
        printf("load history_thread fail: %d", ret);
        return -__LINE__;
    }

    ret = read_cfg_int(root, "assets_thread", &settings.assets_thread, false, 10);
    if (ret < 0)
    {
        printf("load assets_thread fail: %d", ret);
        return -__LINE__;
    }
    ret = read_cfg_int(root, "markets_thread", &settings.markets_thread, false, 10);
    if (ret < 0)
    {
        printf("load markets_thread fail: %d", ret);
        return -__LINE__;
    }

    ERR_RET_LN(read_cfg_real(root, "cache_timeout", &settings.cache_timeout, false, 0.45));

    return 0;
}

int init_config(const char *path)
{
    json_error_t error;
    json_t *root = json_load_file(path, 0, &error);
    if (root == NULL) {
        printf("json_load_file from: %s fail: %s in line: %d\n", path, error.text, error.line);
        return -__LINE__;
    }
    if (!json_is_object(root)) {
        json_decref(root);
        return -__LINE__;
    }

    int ret = read_config_from_json(root);
    if (ret < 0) {
        json_decref(root);
        return ret;
    }
    json_decref(root);

    return 0;
}

