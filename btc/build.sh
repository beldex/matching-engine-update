#! /bin/bash

# 读取配置
cflags=$(/usr/bin/mysql_config --cflags)
libs=$(/usr/bin/mysql_config --libs)
# 替换/为转义,sed要用
cflags=${cflags//\//\\\/}
libs=${libs//\//\\\/}

# 拼接sed命令参数
c="s/INCS =/INCS =${cflags}/g"
l="s/LIBS =/LIBS =${libs}/g"

cd  /src/src/matchengine
sed -i "$c" makefile
sed -i "$l" makefile
make

cd  /src/src/marketprice/
make

cd  /src/src/accesshttp/
make

cd /src/src/accessws
make

cd /src/src/alertcenter
make

cd /src/src/readhistory
sed -i "$c" makefile
sed -i "$l" makefile
make
