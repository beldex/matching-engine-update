create database markets_add;
USE markets_add;

CREATE TABLE `markets` (
    `id`             INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name`           VARCHAR(30) NOT NULL,
    `fee_prec`       INT UNSIGNED NOT NULL,
    `min_amount`     FLOAT NOT  NULL,
    `stock`          VARCHAR(30) NOT NULL,
    `stock_prec`     INT UNSIGNED NOT NULL,
    `money`          VARCHAR(30) NOT NULL,
    `money_prec`     INT UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO markets (name, fee_prec, min_amount, stock, stock_prec, money, money_prec)
        VALUES
                ("ETHBTC",4,0.001,"ETH",6,"BTC",5),
                ("LTCBTC",4,0.15,"LTC",6,"BTC",6),
                ("DASHBTC",4,0.01,"DASH",6,"BTC",5),
                ("BDXBTC",4,50,"BDX",3,"BTC",8),
                ("TREEPBTC",4,20,"TREEP",3,"BTC",8),
                ("BLURTBTC",4,10,"BLURT",3,"BTC",8),
                ("BTCUSDT",4,0.001,"BTC",6,"USDT",2),
                ("BDXUSDT",4,50,"BDX",3,"USDT",3),
                ("ETHUSDT",4,0.001,"ETH",6,"USDT",2),
                ("ZRXUSDT",4,1.5,"ZRX",4,"USDT",4),
                ("PQTUSDT",4,100,"PQT",3,"USDT",8),
                ("LINKUSDT",4,0.15,"LINK",3,"USDT",3),
                ("BATUSDT",4,5,"BAT",4,"USDT",4),
                ("SNTUSDT",4,5,"SNT",4,"USDT",4),
                ("BANDUSDT",4,0.15,"BAND",4,"USDT",2),
                ("COMPUSDT",4,0.002,"COMP",3,"USDT",4),
                ("LRCUSDT",4,5,"LRC",3,"USDT",4),
                ("1INCHUSDT",4,5,"1INCH",3,"USDT",6),
                ("GRTUSDT",4,10,"GRT",4,"USDT",6),
                ("SUSHIUSDT",4,5,"SUSHI",3,"USDT",4),
                ("CHZUSDT",4,0.001,"CHZ",3,"USDT",4),
                ("MANAUSDT",4,0.001,"MANA",3,"USDT",4),
                ("UNIUSDT",4,5,"UNI",3,"USDT",5),
                ("TREEPUSDT",4,50,"TREEP",3,"USDT",8),
                ("BTCIDRT",4,0.001,"BTC",6,"IDRT",2);
