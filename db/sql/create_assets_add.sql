create database assets_add;
USE assets_add;

CREATE TABLE `assets` (
    `id`            INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name`          VARCHAR(30) NOT NULL,
    `prec_save`     INT UNSIGNED NOT NULL,
    `prec_show`     INT UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO assets (name, prec_save, prec_show)
        VALUES
                ("ETH", 16,8),
                ("BTC", 16,8),
                ("XMR", 16,8),
                ("LTC", 16,8),
                ("DASH", 16,8),
                ("BDX", 16,8),
                ("USDT", 16,2),
                ("ZRX", 18,8),
                ("IDRT", 16,2),
                ("LINK", 16,8),
                ("BAND", 16,8),
                ("BAT", 16,8),
                ("PQT", 16,8),
                ("COMP", 16,8),
                ("THETA", 16,8),
                ("1INCH", 16,8),
                ("GRT", 16,8),
                ("SUSHI", 16,8),
                ("UNI", 16,8),
                ("UCA", 16,8),
                ("TREEP", 16,8),
                ("BTCX", 16,8),
                ("BLURT", 16,8),
                ("LRC", 16,8),
                ("CHZ", 16,8),
                ("MANA", 16,8),
                ("SNT", 16,8);
